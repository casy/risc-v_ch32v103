/*
 * board.c
 *
 *  Created on: Nov 27, 2020
 *      Author: casy
 */



/*******************************************************************************
* Function Name  : GPIO_Toggle_INIT
* Description    : Initializes GPIOA.0
* Input          : None
* Return         : None
*******************************************************************************/


#include "board.h"


void BSP_LED_init(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
}


int spi_flash_test(void)
{
    const u8 TEXT_Buf[]={"CH32F103 SPI FLASH W25Qxx"};

    int SIZE =  sizeof(TEXT_Buf);

    u8 datap[SIZE];
	u16 Flash_Model;

	SPI_Flash_Init();

	Flash_Model = SPI_Flash_ReadID();

	switch(Flash_Model)
	{
		case W25Q80:
			printf("W25Q80 OK!\r\n");

			break;

		case W25Q16:
			printf("W25Q16 OK!\r\n");

			break;

		case W25Q32:
			printf("W25Q32 OK!\r\n");

			break;

		case W25Q64:
			printf("W25Q64 OK!\r\n");

			break;

		case W25Q128:
			printf("W25Q128 OK!\r\n");

			break;

		default:
			printf("Fail!\r\n");

		  break;

	}
	printf("Start Erase W25Qxx....\r\n");
	SPI_Flash_Erase_Sector(0);
	printf("W25Qxx Erase Finished!\r\n");

	Delay_Ms(500);
	printf("Start Read W25Qxx....\r\n");
	SPI_Flash_Read(datap,0x0,SIZE);
	printf("%s\r\n", datap );

	Delay_Ms(500);
	printf("Start Write W25Qxx....\r\n");
	SPI_Flash_Write((u8*)TEXT_Buf,0,SIZE);
	printf("W25Qxx Write Finished!\r\n");

	Delay_Ms(500);
	printf("Start Read W25Qxx....\r\n");
	SPI_Flash_Read(datap,0x0,SIZE);
	printf("%s\r\n", datap );

	return 0;
}


