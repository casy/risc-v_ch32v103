/*
 * board.h
 *
 *  Created on: Nov 27, 2020
 *      Author: casy
 */

#ifndef BSP_BOARD_H_
#define BSP_BOARD_H_


#include "bsp_spiflash.h"



void BSP_LED_init(void);

int spi_flash_test(void);


#endif /* BSP_BOARD_H_ */
