/*
 * bsp_spiflash.h
 *
 *  Created on: Nov 27, 2020
 *  Author: casy
 */

#ifndef BSP_SPIFLASH_H_
#define BSP_SPIFLASH_H_


#include "ch32v10x.h"
#include "debug.h"

/* Winbond SPIFalsh ID */
#define W25Q80 	0XEF13
#define W25Q16 	0XEF14
#define W25Q32 	0XEF15
#define W25Q64 	0XEF16
#define W25Q128	0XEF17

/* Winbond SPIFalsh Instruction List */
#define W25X_WriteEnable		  	0x06
#define W25X_WriteDisable		  	0x04
#define W25X_ReadStatusReg			0x05
#define W25X_WriteStatusReg			0x01
#define W25X_ReadData			    0x03
#define W25X_FastReadData		  	0x0B
#define W25X_FastReadDual		  	0x3B
#define W25X_PageProgram		  	0x02
#define W25X_BlockErase			  	0xD8
#define W25X_SectorErase		  	0x20
#define W25X_ChipErase			  	0xC7
#define W25X_PowerDown			  	0xB9
#define W25X_ReleasePowerDown		0xAB
#define W25X_DeviceID			    0xAB
#define W25X_ManufactDeviceID		0x90
#define W25X_JedecDeviceID			0x9F



void SPI_Flash_Init(void);

void SPI_Flash_Read(u8* pBuffer,u32 ReadAddr,u16 size);

void SPI_Flash_Erase_Sector(u32 Dst_Addr);

void SPI_Flash_Write_Page(u8* pBuffer,u32 WriteAddr,u16 size);

void SPI_Flash_Write(u8* pBuffer,u32 WriteAddr,u16 size);


u16 SPI_Flash_ReadID(void);



#endif /* BSP_BOARD_H_ */
