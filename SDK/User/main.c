/********************************** (C) COPYRIGHT *******************************
* File Name          : main.c
* Author             : WCH
* Version            : V1.0.0
* Date               : 2020/04/30
* Description        : Main program body.
*******************************************************************************/

/*
 *@Note
 串口打印调试例程：
 USART1_Tx(PA9)。
 本例程演示使用 USART1(PA9) 作打印调试口输出。

*/

#include "debug.h"
#include "board.h"


/*******************************************************************************
* Function Name  : main
* Description    : Main program.
* Input          : None
* Return         : None
*******************************************************************************/
int main(void)
{
    u8 i = 0;
	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
    Delay_Init();

	BSP_LED_init();
	
    USART_Printf_Init(115200);
	
    printf("SystemClk:%d\r\n",SystemCoreClock);

    printf("This is printf example\r\n");

	spi_flash_test();

	 while(1)
	{
		  Delay_Ms(250);
		  GPIO_WriteBit(GPIOA, GPIO_Pin_0, (i==0) ? (i=Bit_SET):(i=Bit_RESET));
	 }


}

